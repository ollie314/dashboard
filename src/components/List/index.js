import React from "react";
import {useHistory} from 'react-router-dom';

import {Table} from 'antd'

const List = ({items, nbItemPerPage, columns, resourceName}) => {
  const history = useHistory();

  const paginationConfig = {
    pageSize: nbItemPerPage,
    position: ['bottomRight', 'topRight'],
  };

  return (
    <Table
      pagination={paginationConfig}
      columns={columns} dataSource={items} onRow={(record, rowIndex) => {
      return {
        onClick: _ => {
          history.push(`/${resourceName}/${record.id}`);
        }
      };
    }}/>
  );
};

export default List;
