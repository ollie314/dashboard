import React from 'react';
import {Tag} from "antd";

const Tags = ({tags}) => {
  return (<>
    {tags.map(({color, name}) => {
      return (
        <Tag color={color} key={name}>
          {name}
        </Tag>
      );
    })}
  </>);
};

export default Tags;
