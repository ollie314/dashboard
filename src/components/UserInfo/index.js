import React from "react";

import { Descriptions } from 'antd';

const UserInfo = ({user}) => {

  return (
    <Descriptions title="Informations" bordered>
      <Descriptions.Item label="Partner id">{user.partnerId}</Descriptions.Item>
      <Descriptions.Item label="First name">{user.firstname}</Descriptions.Item>
      <Descriptions.Item label="Last name">{user.lastname}</Descriptions.Item>
      <Descriptions.Item label="Phone">{user.phone}</Descriptions.Item>
      <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
    </Descriptions>
  );
};

export default UserInfo;
