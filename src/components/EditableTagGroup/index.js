import React, {useState} from 'react';
import { Tag, Input,  Row, Col } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

const EditableTagGroup = ({tags, setTags}) => {
  const [inputValue, setInputValue] = useState('');
  const [inputVisible, setInputVisible] = useState(false);

  let input;

  const handlerRemoveTag = (t) => {
    const ts = tags.filters(_t => _t !== t );
    setTags(ts);
  };

  const forMap = tag => {
    const tagElem = (
      <Tag
        closable
        onClose={e => {
          e.preventDefault();
          handlerRemoveTag(tag);
        }}
      >
        {tag}
      </Tag>
    );
    return (
      <span key={tag} style={{ display: 'inline-block' }}>
        {tagElem}
      </span>
    );
  };

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const saveInputRef = (ipt) => {
    input = ipt;
  };

  const showInput = () => {
    setInputVisible(true);
  };

  const handleInputConfirm = () => {
    if(inputValue && tags.indexOf(inputValue) === -1)
      setTags([...tags, inputValue]);
    setInputVisible(false);
    setInputValue('');
  };

  const tagChild = tags.map(forMap);

  return (
    <div>
      <Row gutter={16}>
        <Col className="gutter-row" span={6}>
          {tagChild}
        </Col>
      </Row>
      {inputVisible && (
        <Input
          ref={saveInputRef}
          autoFocus={true}
          type="text"
          size="small"
          style={{ width: 78 }}
          value={inputValue}
          onChange={handleInputChange}
          onBlur={handleInputConfirm}
          onPressEnter={handleInputConfirm}
        />
      )}
      {!inputVisible && (
        <Row gutter={16}>
          <Col className="gutter-row" span={6}>
            <Tag onClick={showInput} className="site-tag-plus">
              <PlusOutlined /> New Tag
            </Tag>
          </Col>
        </Row>
      )}
    </div>
  );
}

export default EditableTagGroup;
