import React from 'react';
import {Popover, Table, Typography} from "antd";

const { Title } = Typography;

const columns = [
  {
    title: 'Title',
    dataIndex: 'title',
  },
  {
    title: 'Type',
    dataIndex: 'type',
  },
  {
    title: 'Required',
    dataIndex: 'required',
    render: t => {
      if(t) {return "true"}
      return "false"
    }
  },
];

const extractSchema = (schema) => {
  const isRequired = (t) => schema.required.includes(t);
  const {properties} = schema;
  const data = [];
  Object.keys(properties).forEach((k, i) => {
    const {type} = properties[k];
    data.push({title: k, type, required: isRequired(k), key: i});
  });
  return data;
};

const SchemaPaylaod = ({schema}) => {
  const data = extractSchema(schema);
  return (
    <Table columns={columns}
           dataSource={data}
           size='small'
           pagination={{ position: ["none", "none"] }}/>
  )
};

const PayloadDataTableCell = ({payload}) => {
  const {name, schema} = payload;
  const scm = JSON.parse(schema);
  const content = <SchemaPaylaod schema={scm} />;
  const title = <Title level={4}>{name}</Title>;
  return (
    <Popover content={content} title={title}>
      <span>{name}</span>
    </Popover>
  )
};

export default PayloadDataTableCell;
