import React, {useState} from 'react';
import { Form, Input, Button, Space } from 'antd';
import { generateEmptyUser } from "../../lib/userService";

const layout = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 4,
    span: 16,
  },
};

const noop = () => {};

const UserForm = ({user = null, onComplete = noop, onCancel = noop, saveDelegate = noop, setLoading = noop}) => {
  const [usr] = useState(user || generateEmptyUser());

  const onFinish = async values => {
    setLoading(true);
    // TODO: save the user using the webservice
    try {
      // try to save the user
      /*const r = */ await saveDelegate(values);
      // TODO: manage the response of the service
    } catch(err) {
      // modal message to inform use about the error
    }
    setLoading(false);
    onComplete();
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form {...layout}
           name="userForm"
           onFinish={onFinish}
           onFinishFailed={onFinishFailed}>
      <Form.Item
        label="Partner ID"
        name="partnerId"
        initialValue={usr.partnerId || 1234}
        rules={[
          {
            required: true,
            message: 'Please input the unique id of the partner!',
          },
        ]}
      >
        <Input placeholder="Enter the partner ID" />
      </Form.Item>
      <Form.Item
        label="Firstname"
        name="firstname"
        initialValue={usr.firstname}
        rules={[
          {
            required: true,
            message: 'Please input the first name of the user!',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Last name"
        name="lastname"
        initialValue={usr.lastname}
        rules={[
          {
            required: true,
            message: 'Please input the last name of the user!',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Phone"
        name="phone"
        initialValue={usr.phone}
        rules={[
          {
            required: true,
            message: 'Please input the last name of the phone!',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Email"
        name="email"
        initialValue={usr.email}
        rules={[
          {
            required: true,
            message: 'Please input the last name of the email!',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Space>
          <Button onClick={() => {
            onCancel()
          }}>
            Cancel
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default UserForm;
