import React from 'react';
import { Form, Input, Button, Space } from 'antd';

const layout = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 4,
    span: 16,
  },
};

const noop = () => {};

// Resource, resource endpoint, fields + labels
const InvoiceForm = ({invoice = null, onComplete = noop, onCancel = noop, saveDelegate = noop, setLoading = noop}) => {

  const onFinish = async values => {
    setLoading(true);
    // TODO: save the user using the webservice
    try {
      // try to save the user
      /*const r = */await saveDelegate(values);
      // TODO: manage the response of the service
    } catch(err) {
      // modal message to inform use about the error
    }
    setLoading(false);
    onComplete();
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form {...layout}
          name="userForm"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}>
      <Form.Item
        label="Invoice iD"
        name="partnerId"
        initialValue={invoice.id}
        rules={[
          {
            required: true,
            message: 'Please input the unique id of the partner!',
          },
        ]}
      >
        <Input placeholder="Enter the partner ID" />
      </Form.Item>
      <Form.Item
        label="Insured number"
        name="insuredNumber"
        initialValue={invoice.insuredNumber}
        rules={[
          {
            required: true,
            message: 'Please input the unique id of the partner!',
          },
        ]}
      >
        <Input placeholder="Enter the insured ID" />
      </Form.Item>
      <Form.Item
        label="Source"
        name="source"
        initialValue={invoice.source}
        rules={[
          {
            required: true,
            message: 'Please input the source',
          },
        ]}
      >
        <Input placeholder="Enter the source" />
      </Form.Item>
      <Form.Item
        label="Document iD"
        name="documentId"
        initialValue={invoice.documentId}
        rules={[
          {
            required: true,
            message: 'Please input the unique id of the document!',
          },
        ]}
      >
        <Input placeholder="Enter the document ID" />
      </Form.Item>
      <Form.Item
        label="Creation date"
        name="creationDate"
        initialValue={invoice.creationDate}
        rules={[
          {
            required: true,
            message: 'Please input the creation date!',
          },
        ]}
      >
        <Input placeholder="Enter the creation date" />
      </Form.Item>
      <Form.Item
        label="Domain"
        name="domain"
        initialValue={invoice.domain}
        rules={[
          {
            required: true,
            message: 'Please input the domain!',
          },
        ]}
      >
        <Input placeholder="Enter the domain" />
      </Form.Item>
      <Form.Item
        label="Notification type"
        name="notificationType"
        initialValue={invoice.notificationType}
        rules={[
          {
            required: true,
            message: 'Please input the type of notification!',
          },
        ]}
      >
        <Input placeholder="Enter the type of notification" />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Space>
          <Button onClick={() => {
            onCancel()
          }}>
            Cancel
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default InvoiceForm;
