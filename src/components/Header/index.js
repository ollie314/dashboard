import React from "react";
import { useHistory } from "react-router-dom";

import { Menu } from 'antd';

const Header = () => {
  const history = useHistory();

  const navigate = (url) => {
    history.push(url)
  };

  return (
    <>
      <div className="logo" />
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
        <Menu.Item key="1" onClick={() => {navigate("/")}}>Home</Menu.Item>
        <Menu.Item key="2" onClick={() => {navigate("/users")}}>Users</Menu.Item>
        <Menu.Item key="3" onClick={() => {navigate("/invoices")}}>Invoices</Menu.Item>
        <Menu.Item key="4" onClick={() => {navigate("/tracking-board")}}>Tracking Dashboard</Menu.Item>
        <Menu.Item key="5" onClick={() => {navigate("/events-wall")}}>Event Wall</Menu.Item>
        <Menu.Item key="6" onClick={() => {navigate("/settings")}}>Settings</Menu.Item>
      </Menu>
    </>
  );
};

export default Header;
