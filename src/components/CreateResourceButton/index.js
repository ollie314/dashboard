import React from 'react';
import { Link } from "react-router-dom";

import { Button } from 'antd'

const CreateResourceButton = ({resource}) => {
  return (
    <Button>
      <Link type="button" to={`/${resource}/new`}>Create new</Link>
    </Button>
  );
};

export default CreateResourceButton;
