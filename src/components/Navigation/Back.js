import React from 'react';
import { withRouter } from "react-router-dom";

const goBack = ({history}) => <img className ="go-back" src="https://cdn.iconscout.com/icon/premium/png-512-thumb/go-back-2-894878.png" onClick={() => history.goBack()} alt="back" />;

export default withRouter(goBack);
