import { withRouter } from 'react-router-dom'

const ButtonToNavigate = ({ history, url = '/' }) => (
  <span
    onClick={() => history.push(url)}
  >
    Navigate
  </span>
);

export default withRouter(ButtonToNavigate);
