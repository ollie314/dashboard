import React from "react";

const Footer = () => {
  return (
    <span style={{ textAlign:'center'}}>
      Management for stream based notifications
    </span>
  );
};

export default Footer;
