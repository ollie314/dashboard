import React from 'react';
import { Descriptions } from 'antd';

const InvoiceInfo = ({invoice}) => {
  return (
    <Descriptions title="Informations" bordered>
      <Descriptions.Item label="InvoiceId">{invoice.id}</Descriptions.Item>
      <Descriptions.Item label="Insured number">{invoice.insuredNumber}</Descriptions.Item>
      <Descriptions.Item label="Source">{invoice.source}</Descriptions.Item>
      <Descriptions.Item label="Document Id">{invoice.documentId}</Descriptions.Item>
      <Descriptions.Item label="Creation Date">{invoice.creationDate}</Descriptions.Item>
      <Descriptions.Item label="Domain">{invoice.domain}</Descriptions.Item>
      <Descriptions.Item label="Notification type">{invoice.notificationType}</Descriptions.Item>
    </Descriptions>
  );
};

export default InvoiceInfo;
