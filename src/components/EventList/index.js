import React, {useState, useEffect} from 'react';

import {fetchAllEvents} from "../../lib/eventService";

import List from "../List";
import PayloadDataTableCell from "../PayloadDataTableCell";
import Tags from "../Tags";
import TableAction from "../TableAction";

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Domain (owner)',
    dataIndex: 'domain',
    key: 'domain',
  },
  {
    title: 'Short decription',
    dataIndex: 'description',
    key: 'description',
  },
  {
    title: 'Payload',
    dataIndex: 'payload',
    key: 'payload',
    render: p => (
      <PayloadDataTableCell payload={p} />
    )
  },
  {
    title: 'Tags',
    dataIndex: 'tags',
    key: 'tags',
    render: tags => (
      <Tags tags={tags} />
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <TableAction record={record} text={text} />
    ),
  },
];

const EventList = () => {
  const [events, setEvents] = useState([]);

  useEffect(() => {
    const f = async () => {
      const results = await fetchAllEvents();
      setEvents(results);
    };
    f().catch((e) => {
      // TODO: create an alert on the UI
    });
  }, []);

  return (<>
    <List columns={columns}
          items={events.map( ({...e}, i) => ({...e, key: i}))}
          nbItemPerPage={5}
          page={1}
          resourceName={'events'} />
  </>)
};

export default EventList;
