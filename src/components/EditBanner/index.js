import React from 'react';
import { useHistory } from 'react-router-dom';

import {Button, PageHeader} from "antd";
import {editing} from "../../lib/utils";
import {Link} from "react-router-dom";

const EditBanner = ({resource, resourceId, title, subtitle, edit, onBack = null}) => {
  const history = useHistory();
  const extra = [];
  if(!editing(edit)) {
    extra.push(
      <Button key="1" type="primary">
        <Link to={`/${resource}/${resourceId}?edit=1`}>Edit</Link>
      </Button>
    );
  } else {
    extra.push(
      <Button key="1">
        <Link to={`/${resource}/${resourceId}`}>Cancel</Link>
      </Button>
    );
  }
  extra.push(<Button key="2">
    <Link to={`/${resource}`}>Back to list</Link>
  </Button>)

  const defaultOnBack = () => {
    history.goBack();
  };

  const backHandler = onBack || defaultOnBack;

  return (
    <PageHeader
      className="site-page-header"
      onBack={backHandler}
      title={title}
      subTitle={subtitle}
      extra={extra}
    />
  );
};

export default EditBanner;
