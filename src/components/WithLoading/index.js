import React, { useState } from 'react';

import { Spin } from 'antd';

const WithLoading = (WrappedItem, WaitContent = null) => {
  const Hoc = ({loadingTip = 'loading', isLoading = false, ...props}) => {
    const [ loading, setLoading ] = useState(isLoading);
    const newProps = {...props, loading, setLoading};

    if(loading) {
      return (<Spin tip={loadingTip}>
        {WaitContent === null && <WrappedItem {...newProps} /> }
        {WaitContent !== null && <WaitContent />}
      </Spin>);
    }

    return (
      <WrappedItem {...newProps} />
    );
  };
  return Hoc;
}

export default WithLoading;
