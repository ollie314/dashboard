import React, {useState} from 'react';
import {Button, Tooltip, Drawer} from "antd";
import { PlusOutlined } from '@ant-design/icons';
import EventForm from "../EventForm";

const EventButton = () => {
  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(true);
  }

  const onDrawerClose = () => {
    setVisible(false);
  };

  return (
    <>
      <Tooltip title="Create Event">
        <Button
          type="primary"
          shape="circle"
          onClick={showDrawer}
          icon={<PlusOutlined/>}/>
      </Tooltip>
      <Drawer
        title="Create new Event"
        width={720}
        onClose={onDrawerClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          <div
            style={{
              textAlign: 'right',
            }}
          >
            <Button onClick={onDrawerClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button onClick={onDrawerClose} type="primary">
              Submit
            </Button>
          </div>
        }
      >
        <EventForm />
      </Drawer>
    </>
  );
};

export default EventButton;
