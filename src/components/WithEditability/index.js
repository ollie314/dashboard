import React from 'react';

import {editing} from "../../lib/utils";

const WithEditing = (EditingComponent, DisplayComponent) => {
  const Hoc = ({edit, ...props}) => {
    if (editing(edit)) {
      return (<EditingComponent {...props}/>);
    }
    return (<DisplayComponent {...props} />);
  };
  return Hoc;
};

export default WithEditing;
