import React, { useState } from 'react';
import {Form, Col, Row, Input, Select, Divider, Typography} from 'antd';
import MonacoEditor from "react-monaco-editor";

import EditableTagGroup from "../EditableTagGroup";
import {defaultSchema} from "../../lib/eventService";

const { Option } = Select;
const { Title } = Typography;

const defaultSchemaString = JSON.stringify(defaultSchema, null, 2);

const EventForm = () => {
  const [code, setCode] = useState(defaultSchemaString);
  const [tags, setTags] = useState([]);

  const options = {
    selectOnLineNumbers: true
  };

  return (
    <Form layout="vertical" hideRequiredMark>
      <Row gutter={16}>
        <Col span={12}>
          <Form.Item
            name="name"
            label="Name"
            rules={[{ required: true, message: 'Please enter the name of the event' }]}
          >
            <Input placeholder="Please enter the name of the event" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="domain"
            label="Domain"
            rules={[{ required: true, message: 'Please enter domain' }]}
          >
            <Input
              placeholder="Please enter the domain of the event"
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={12}>
          <Form.Item
            name="owner"
            label="Owner"
            rules={[{ required: true, message: 'Please select an owner' }]}
          >
            <Select placeholder="Please select an owner">
              <Option value="xiao">Xiaoxiao Fu</Option>
              <Option value="mao">Maomao Zhou</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="type"
            label="Type"
            rules={[{ required: true, message: 'Please choose the type' }]}
          >
            <Select placeholder="Please choose the type">
              <Option value="private">Private</Option>
              <Option value="public">Public</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <Form.Item
            name="description"
            label="Description"
            rules={[
              {
                required: true,
                message: 'please enter url description',
              },
            ]}
          >
            <Input.TextArea rows={4} placeholder="please enter url description" />
          </Form.Item>
        </Col>
      </Row>
      <Divider />
      <Row gutter={16}>
        <Col span={24}>
          <EditableTagGroup tags={tags} setTags={setTags} />
        </Col>
      </Row>
      <Divider />
      <Row gutter={16}>
        <Col span={24}>
          <Title level={3}>Describe payload of the event (jsonSchema)</Title>
          <MonacoEditor
            width="100%"
            roundedSelection={false}
            height="500"
            defaultValue="{}"
            language="json"
            theme="vs-dark"
            value={code}
            options={options}
            onChange={c => {
              setCode(c);
            }}
          />
        </Col>
      </Row>
      <Divider />
    </Form>
  )
};

export default EventForm;
