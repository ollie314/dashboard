import React from 'react';
import {Space} from "antd";

const TableAction = ({record, text}) => {
  return (<Space size="middle">
    <a href="javascript; void(0)">Info</a>
    <a href="javascript; void(0)">Details</a>
    <a href="javascript; void(0)">Delete</a>
  </Space>)
};

export default TableAction;
