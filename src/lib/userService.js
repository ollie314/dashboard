import { partnerIdGenerator as generatePartnerId } from './generator';

// service
export const generateEmptyUser = () => ({
  partnerId: generatePartnerId(),
  firstname: '',
  lastname: '',
  phone: '',
  email: ''
});

// ports for rest / graphql management
export const fetchUsers = async ({q}) => {}
export const fetchUserById = async (id) => {};

export const createUser = async (user) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, 1e3);
  });
};

export const updateUser = async (id, user) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, 1e3);
  });
};
export const deleteUser = async (id) => {};
