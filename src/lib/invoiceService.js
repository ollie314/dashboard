export const generateInvoice = () => {
  const id = Math.round(Math.random() * 1000) + 1;
  return {
    id,
    insuredNumber: '',
    source: '',
    documentId: '',
    creationDate: '',
    domain: '',
    notificationType: ''
  };
};
const invoices = [
  {
    "id": 1,
    "insuredNumber":2969519,
    "source":"dms-api",
    "documentId":"MET_CON1575370414889_6703",
    "creationDate":"2020-06-01T10:50:18Z",
    "domain":"MET_CON",
    "notificationType":"EDITIQUE"
  },
  {
    "id": 2,
    "insuredNumber":234785,
    "source":"fin-api",
    "documentId":"MET_CON1575370414889_5443",
    "creationDate":"2020-07-03T08:36:54Z",
    "domain":"MET_CON",
    "notificationType":"FACTURATION"
  },
];

export const fetchInvoices = async (page = 0, pageSize = 5) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(invoices.slice(page, page + pageSize));
    }, 1.3e3);
  });
};

export const fetchInvoiceById = async (id) => {
  return new Promise((resolve, reject) => {
    setTimeout( () => {
      const results = invoices.filter(({id: iid}) => iid === parseInt(id));
      if(results.length) {
        resolve(results[0]);
      } else {
        reject(new Error('invoice not found'));
      }
    }, 1e3);
  });
};

export const createInvoice = async (invoice) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(invoice);
    }, 1e3);
  });
};

export const updateInvoice = async (id, invoice) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, 1e3);
  });
};

export const deleteInvoice = async (id) => {};
