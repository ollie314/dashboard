/**
 * Load the list of settings
 *
 * @returns {Promise<void>}
 */
export const fetchSettings = async () => {};

/**
 * Fetch a setting with a given name
 *
 * @param name the name of the settings to fetch
 * @returns {Promise<void>}
 */
export const fetchSettingByName = async (name) => {};

/**
 * Update the whole settings page
 *
 * @param settings the list of settings
 * @returns {Promise<void>}
 */
export const updateSettings = async (settings) => {};

/**
 * Update setting with a given key with given value.
 *
 * @param name the name of the settings to update
 * @param value the value to set for the name
 * @returns {Promise<void>}
 */
export const patchSettings = async (name, value) => {};
