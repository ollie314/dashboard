export const defaultSchema = {
  type: 'object',
  required: [
    'id', 'ts'
  ],
  properties: {
    id: {
      type: 'string'
    },
    ts: {
      type: 'string',
      format: 'date-time'
    },
    correlationId: {
      type: 'string',
      format: '^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$'
    }
  }
};

const notificationDataSchema = {
  type: 'object',
  required: [
    'id', 'sender', 'recipient'
  ],
  properties: {
    id: {
      type: "string"
    },
    sender: {
      type: "string"
    },
    recipient: {
      type: "string"
    }
  }
}

const events = [
  {
    id: '62eb5f3c-ec5f-4b37-bbbf-2a187822998f',
    name: 'Notification',
    domain: 'gce',
    description: 'notification sent by GCE when it sends notification to insureds',
    category: 'notifications',
    payload: {
      name: 'NotificationData',
      schema: JSON.stringify(notificationDataSchema)
    },
    tags: [
      {name: 'gce', color: 'blue'},
      {name: 'notification', color: 'green'},
      {name: 'integration', color: 'gray'},
    ]
  }
];

/**
 * Fetch the list of events from the backend
 * @returns {Promise<[{payload: {schema: string, name: string}, domain: string, name: string, description: string, id: string, category: string, tags: [{color: string, name: string}, {color: string, name: string}, {color: string, name: string}]}]>}
 */
export const fetchAllEvents = async () => {
  return Promise.resolve(events);
};

/**
 * Fetch an event using his id
 *
 * @param id the unique id of the event
 * @returns {Promise<{}>}
 */
export const fetchEventById = async (id) => {
  return Promise.resolve({});
};

/**
 * Create a new event on the system
 *
 * @param event the paylaod of the event to create
 * @returns {Promise<{}>}
 */
export const createEvent = async (event) => {
  return Promise.resolve({});
};

/**
 * Update an event given the id of the event and the new event's payload
 *
 * @param id the unique id of the event
 * @param event the new payload of the event
 * @returns {Promise<Event>}
 */
export const updateEvent = async (id, event) => {
  return Promise.resolve(event);
};

/**
 * Delete an event given his id
 *
 * @param id the id of the even to delete
 * @returns {Promise<{}>}
 */
export const deleteEvent = async (id) => {
  return Promise.resolve({});
};

/**
 * Find all events that are similar to the given event
 *
 * @param event the event to check similar event for
 * @returns {Promise<*[Event]>}
 */
export const findSimilarEvent = async  (event) => {
  return Promise.resolve([]);
};

