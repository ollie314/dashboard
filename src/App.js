import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import 'antd/dist/antd.css';

import { Layout} from 'antd';

import AppHeader from './components/Header';
import AppFooter from './components/Footer';

import Users from './containers/Users';
import UserDetails from './containers/UserDetails';
import InvoiceCreation from "./containers/InvoiceCreation";
import Invoices from "./containers/Invoices";
import InvoiceDetails from "./containers/InvoiceDetails";
import UserCreation from "./containers/UserCreation";
import AppBreadcrumb from "./components/AppBreadcrumb";
import Home from "./containers/Home";
import WithLoading from "./components/WithLoading";
import TrackingBoard from "./containers/TrackingBoard";
import Settings from "./containers/Settings";
import EventWall from "./containers/EventWall";

const { Header, Content, Footer } = Layout;

const App = () => {
  const [page, setPage] = useState(0);

  const UserDetailsWithLoading = WithLoading(UserDetails);

  return (
    <Router>
      <Layout className="layout">
        <Header>
          <AppHeader/>
        </Header>
        <Content style={{ padding: '0 50px', backgroundColor: 'white' }}>
          <AppBreadcrumb />
          <Switch>
            <Route path="/events-wall">
              <EventWall />
            </Route>
            <Route path="/settings">
              <Settings />
            </Route>
            <Route path="/tracking-board">
              <TrackingBoard />
            </Route>
            <Route path="/invoices/new">
              <InvoiceCreation />
            </Route>
            <Route path="/invoices/:id">
              <InvoiceDetails />
            </Route>
            <Route path="/invoices">
              <Invoices />
            </Route>
            <Route path="/users/new">
              <UserCreation />
            </Route>
            <Route path="/users/:id">
              <UserDetailsWithLoading />
            </Route>
            <Route path="/users">
              <Users page={page} setPage={setPage}/>
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Content>
        <Footer>
          <AppFooter/>
        </Footer>
      </Layout>
    </Router>
  );
}

export default App;
