import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import {Divider, PageHeader} from 'antd';
import {fetchInvoices} from "../lib/invoiceService";
import List from "../components/List";
import CreateResourceButton from "../components/CreateResourceButton";

const columns = [
  {
    title: 'Insured number',
    dataIndex: 'insuredNumber',
    key: 'insuredNumber',
  },
  {
    title: 'Source',
    dataIndex: 'source',
    key: 'source',
  },
  {
    title: 'Creation date',
    dataIndex: 'creationDate',
    key: 'creationDate',
  },
  {
    title: 'Domain',
    dataIndex: 'domain',
    key: 'doain',
  },
  {
    title: 'Notification type',
    dataIndex: 'notificationType',
    key: 'notificationType',
  },
];

const Invoices = ({page}) => {
  const [invoices, setInvoices] = useState([]);
  const [nbItemPerPage] = useState(5);

  const history = useHistory();

  useEffect(() => {
    let pending = true;
    const f = () => {
      const response = fetchInvoices(page);
      response.then((results) => {
        if(pending) {
          setInvoices(results.map((r, i) => ({...r, key: i})));
        }
      });
    }
    f();
    return () => { pending = false; }
  }, [page, nbItemPerPage]);

  return (
    <div>
      <PageHeader
        className="site-page-header"
        onBack={() => {
          history.goBack();
        }}
        title="Invoices"
        subTitle="Show / manager invoices"
        />
      <List items={invoices} page={page} nbItemPerPage={nbItemPerPage} columns={columns} resourceName={"invoices"}/>
      <CreateResourceButton resource="invoices" />
      <Divider />
    </div>
  );
};

export default Invoices;
