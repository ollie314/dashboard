import React, { useState, useEffect } from "react";
import {useHistory} from 'react-router-dom';
import { Divider, PageHeader } from 'antd';

import List from '../components/List';
import CreateResourceButton from "../components/CreateResourceButton";

const columns = [
  {
    title: 'Firstname',
    dataIndex: 'firstname',
    key: 'firstname',
  },
  {
    title: 'Lastname',
    dataIndex: 'lastname',
    key: 'lastname',
  },
  {
    title: 'Phone',
    dataIndex: 'phone',
    key: 'phone',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
];

const Users = ({page}) => {
  const [users, setUsers] = useState([]);
  const [nbItemPerPage] = useState(5);

  const history = useHistory();

  useEffect(() => {
    const url = "https://jsonplaceholder.typicode.com/users";
    fetch(url)
    .then(response => response.json())
    .then(p => p.map( u => {
        const {name, id} = u;
        const [firstname, lastname] = name.split(' ');
        return {...u, key: id, firstname, lastname};
      })
    )
    .then(json => setUsers(json));
  }, [page, nbItemPerPage]);

  return (
    <div>
      <PageHeader
        className="site-page-header"
        onBack={() => {
          history.goBack();
        }}
        title="Users"
        subTitle="Show / manager Users"
      />
      <List items={users} page={page} nbItemPerPage={nbItemPerPage} columns={columns} resourceName={"users"}/>
      <CreateResourceButton resource="users" />
      <Divider />
    </div>
  );
};

export default Users;
