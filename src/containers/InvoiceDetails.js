import React, {useState, useEffect} from 'react';
import {useParams, useHistory} from 'react-router-dom';

import {Divider} from "antd";

import {fetchInvoiceById, updateInvoice} from "../lib/invoiceService";

import {useQuery} from "../lib/querystring";

import InvoiceInfo from "../components/InvoiceInfo";
import InvoiceForm from "../components/InvoiceForm";
import EditBanner from "../components/EditBanner";
import WithEditing from "../components/WithEditability";

const InvoiceDetails = () => {
  const [invoice, setInvoice] = useState({});
  const history = useHistory();
  const {id} = useParams();

  const {edit = "0"} = useQuery();

  useEffect(() => {
    const f = async () => {
      try {
        const invoice = await fetchInvoiceById(id);
        setInvoice(invoice);
      } catch (e) {
        // alert ...
        history.goBack();
      }
    }
    f();
  }, [id, history]);

  const Component = WithEditing(InvoiceForm, InvoiceInfo);

  return (
    <div>
      <EditBanner
        onBack={() => {
          history.goBack();
        }}
        title="Invoice details"
        resource={"invoices"}
        resourceId={invoice.id}
        subTitle={`Invoice ID: ${invoice.id}`}
        edit={edit}/>

      <Component invoice={invoice}
                 edit={edit}
                 onCancel={() => {
                   history.push(`/invoices/${id}`)
                 }}
                 onComplete={() => {
                   history.push(`/invoices/${id}`)
                 }}
                 saveDelegate={async (inv) => {
                   const r = updateInvoice(inv);
                   return r;
                 }}
      />
      <Divider/>
    </div>
  );
};

export default InvoiceDetails;
