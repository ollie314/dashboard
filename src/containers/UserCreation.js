import React from "react";
import { useHistory } from "react-router-dom";

import { PageHeader } from 'antd';

import UserForm from "../components/UserForm";
import { createUser, generateEmptyUser } from "../lib/userService";
import WithLoading from "../components/WithLoading";

const UserCreation = ({onSuccess, onFailure}) => {
  const history = useHistory();

  const UserFormWithLoading = WithLoading(UserForm);

  return (
    <div>
      <PageHeader
        className="site-page-header"
        onBack={() => {
          history.push("/users");
        }}
        title="User Creation"
        subTitle="Create a new user"
      />
      <UserFormWithLoading onCancel={() => history.goBack()}
                onComplete={() => {history.push("/users")}} // TODO: provide the id of the resource created during the process
                saveDelegate={async (values) => {
                  const r = await createUser(values);
                  return r;
                }}
                user={ generateEmptyUser() }/>
    </div>
  );
};

export default UserCreation;
