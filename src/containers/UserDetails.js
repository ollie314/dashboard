import React, {useState, useEffect} from "react";
import {useParams, useHistory} from "react-router-dom";

import {Divider, Skeleton} from 'antd';

import {useQuery} from "../lib/querystring";
import {updateUser} from "../lib/userService";

import UserInfo from "../components/UserInfo";
import UserForm from "../components/UserForm";
import WithLoading from "../components/WithLoading";

import EditBanner from "../components/EditBanner";
import WithEditing from "../components/WithEditability";

const UserDetails = () => {
  const [user, setUser] = useState({});
  const {id} = useParams();

  const history = useHistory();
  const {edit = "0"} = useQuery();

  useEffect(() => {
    const url = `https://jsonplaceholder.typicode.com/users/${id}`;
    console.log('test')
    fetch(url)
      .then(response => response.json())
      .then(u => {
        const {name} = u;
        const [firstname, lastname] = name.split(' ');
        return {...u, firstname, lastname};
      })
      .then(json => {
        setUser(json);
      });
  }, [id]);

  const UserNestedComponent = WithLoading(WithEditing(UserForm, UserInfo));

  return (
    <div>
      <EditBanner
        onBack={() => {
          history.goBack();
        }}
        title="User details"
        resource={"users"}
        resourceId={user.id}
        subTitle={`User: : ${user.name}`}
        edit={edit}/>
      {!user && <Skeleton />}
      {user && <UserNestedComponent edit={edit}
                           onCancel={() => history.goBack()}
                           onComplete={() => {
                             history.push(`/users/${id}`)
                           }}
                           saveDelegate={async (values) => {
                             const r = await updateUser(id, values);
                             return r;
                           }}
                           user={user}/>}
      <Divider/>
    </div>
  );
};

export default UserDetails;
