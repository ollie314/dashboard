import React from 'react';
import {PageHeader, Divider} from "antd";

import EventList from "../components/EventList";
import EventButton from "../components/EventButton";

const EventWall = () => {
  return (<>
    <PageHeader title="Event Catalog" subTitle="Event management system" />
    <EventList />
    <EventButton />
    <Divider />
  </>);
};

export default EventWall;
