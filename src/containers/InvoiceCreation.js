import React from 'react';
import {useHistory} from 'react-router-dom';
import {PageHeader} from "antd";
import InvoiceForm from "../components/InvoiceForm";
import {createInvoice, generateInvoice} from "../lib/invoiceService";

const InvoiceCreation = () => {
  const history = useHistory();
  return (
    <div>
      <PageHeader
        className="site-page-header"
        onBack={() => {
          history.goBack();
        }}
        title="New Invoice"
        /*subTitle={user.name}
        extra={extra}*/
      />
      <InvoiceForm invoice={generateInvoice()}
                   onCancel={() => {
                     history.push(`/invoices`)
                   }}
                   onComplete={() => {
                     history.push(`/invoices`) // TODO: fix the navigation to go on the newly created record (Take care to availability of the record)
                   }}
                   saveDelegate={async (inv) => {
                     const r = createInvoice(inv);
                     return r;
                   }}/>
    </div>
  );
};

export default InvoiceCreation;
